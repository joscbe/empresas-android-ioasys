package com.example.ioasysapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.ioasysapp.mvp.LoginContrato;
import com.example.ioasysapp.mvp.LoginPresenter;

public class LoginActivity extends AppCompatActivity implements LoginContrato.LoginView {
    private Button btnLogin;
    private EditText edtEmail;
    private EditText edtPassword;

    private TextView tvMessageErro;
    private ConstraintLayout telaCarregando;
    private LoginContrato.LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        presenter = new LoginPresenter(this);

        presenter.start();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = edtEmail.getText().toString();
                String senha = edtPassword.getText().toString();

                presenter.validaLogin(email, senha);
            }
        });
    }

    @Override
    public void bindView() {
        edtEmail = findViewById(R.id.edtTextEmail);
        edtPassword = findViewById(R.id.edtTextPassword);
        btnLogin = findViewById(R.id.btnLogin);
        tvMessageErro = findViewById(R.id.tv_message_error);
        telaCarregando = findViewById(R.id.tela_carregando);

        tvMessageErro.setVisibility(View.GONE);
        telaCarregando.setVisibility(View.GONE);
    }

    @Override
    public void mostrarErro(String message) {
        Toast.makeText(getApplication(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void loginSuccess(Intent intent) {
        startActivity(intent);

        telaCarregando.setVisibility(View.GONE);
    }

    @Override
    public void loginErro() {
        telaCarregando.setVisibility(View.GONE);
        Log.e("login", "deu erro");
        tvMessageErro.setText("Credenciais informadas são inválidas, tente novamente.");
        tvMessageErro.setVisibility(View.VISIBLE);
    }

    @Override
    public void emailErro(String message) {
        edtEmail.setError(message);
        edtEmail.requestFocus();
    }

    @Override
    public void passwordErro(String message) {
        edtPassword.setError(message);
        edtPassword.requestFocus();
    }

    @Override
    public void telaCarregamento() {
        telaCarregando.setVisibility(View.VISIBLE);
    }


}