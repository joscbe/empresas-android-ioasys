package com.example.ioasysapp.config;

import com.example.ioasysapp.interfaces.ApiService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitConfig {
    private final Retrofit retrofit;
    private final String URL = "https://empresas.ioasys.com.br/api/v1/";

    public RetrofitConfig() {
        this.retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public ApiService getApiService(){
        return retrofit.create(ApiService.class);
    }
}
