package com.example.ioasysapp.mvp;

import android.content.Context;
import android.content.Intent;
import android.util.Patterns;

import com.example.ioasysapp.MainActivity;
import com.example.ioasysapp.config.RetrofitConfig;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter implements LoginContrato.LoginPresenter {

    private LoginContrato.LoginView view;

    public LoginPresenter(LoginContrato.LoginView view) {
        this.view = view;
    }

    @Override
    public void start() {
        view.bindView();
    }

    @Override
    public void setView(LoginContrato.LoginView view) {
        this.view = view;
    }

    @Override
    public void validaLogin(String email, String senha) {
        //Metodo que vai validar o login
        if (email.isEmpty()) {
            view.emailErro("Insira um email");
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            view.emailErro("Insira um email valido");
            return;
        }

        if (senha.isEmpty()) {
            view.passwordErro("Insira a senha");
            return;
        }

        if (senha.length() < 6) {
            view.passwordErro("Senha deve ser maior");
            return;
        }

        view.telaCarregamento();

        Call<ResponseBody> call = new RetrofitConfig()
                .getApiService()
                .loginUser(email, senha);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    //Em caso de sucesso
                    if (response.isSuccessful()) {
                        String s = response.body().string();

                        //String header = response.headers().toString();

                        String token = response.headers().get("access-token");
                        String client = response.headers().get("client");
                        String uid = response.headers().get("uid");

                        Intent intent = new Intent((Context) view, MainActivity.class);
                        intent.putExtra("token", token);
                        intent.putExtra("client", client);
                        intent.putExtra("uid", uid);

                        view.loginSuccess(intent);
                    } else {
                        view.loginErro();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //Em caso de falha
                view.mostrarErro(t.getMessage());
            }
        });
    }
}
