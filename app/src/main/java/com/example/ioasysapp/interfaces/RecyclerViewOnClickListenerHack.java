package com.example.ioasysapp.interfaces;

import android.view.View;

public interface RecyclerViewOnClickListenerHack {
    public void onClickListener(View view, int position);
}
