package com.example.ioasysapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ioasysapp.R;
import com.example.ioasysapp.domain.Empresas;
import com.example.ioasysapp.interfaces.RecyclerViewOnClickListenerHack;
import com.squareup.picasso.Picasso;

import java.util.List;

public class EmpresasAdapter extends RecyclerView.Adapter<com.example.ioasysapp.adapter.EmpresasAdapter.MyViewHolder> {
    private List<Empresas> list;
    private LayoutInflater layoutInflater;
    private RecyclerViewOnClickListenerHack recyclerViewOnClickListenerHack;

    public EmpresasAdapter(Context context, List<Empresas> list){
        this.list = list;
        this.layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_empresas, parent, false);
        MyViewHolder mvh = new MyViewHolder(view);
        return mvh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        //TODO:Adicionar o picasso pra enviar a imagem
        //holder.ivEmpresa.setImageResource(R.drawable.logo_ioasys);
        holder.tvNome.setText(list.get(position).getNome());
        holder.tvTipo.setText(list.get(position).getTipo());
        holder.tvPais.setText(list.get(position).getPais());

        Picasso.get()
                .load(list.get(position).getImage())
                .into(holder.ivEmpresa);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r){
        recyclerViewOnClickListenerHack = r;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView ivEmpresa;
        public TextView tvNome;
        public TextView tvTipo;
        public TextView tvPais;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ivEmpresa = itemView.findViewById(R.id.iv_item_empresa);
            tvNome = itemView.findViewById(R.id.nome_item_empresa);
            tvTipo = itemView.findViewById(R.id.tipo_item_empresa);
            tvPais = itemView.findViewById(R.id.pais_item_empresa);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (recyclerViewOnClickListenerHack != null){
                recyclerViewOnClickListenerHack.onClickListener(view, getPosition());
            }
        }
    }
}
