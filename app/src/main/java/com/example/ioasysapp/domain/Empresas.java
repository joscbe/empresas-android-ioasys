package com.example.ioasysapp.domain;

import java.io.Serializable;

public class Empresas implements Serializable {
    private String nome;
    private String tipo;
    private String pais;
    private String image;
    private String descricao;
    private final String URL_FOTO = "https://empresas.ioasys.com.br";

    public Empresas(String nome, String tipo, String pais, String image, String descricao) {
        this.nome = nome;
        this.tipo = tipo;
        this.pais = pais;
        this.image = image;
        this.descricao = descricao;
    }

    public String getNome() {
        return nome;
    }

    public String getTipo() {
        return tipo;
    }

    public String getPais() {
        return pais;
    }

    public String getImage() {
        return URL_FOTO+image;
    }

    public String getDescricao() {
        return descricao;
    }
}
