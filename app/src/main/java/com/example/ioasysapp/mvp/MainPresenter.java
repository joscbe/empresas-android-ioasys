package com.example.ioasysapp.mvp;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.example.ioasysapp.DetalhesActivity;
import com.example.ioasysapp.adapter.EmpresasAdapter;
import com.example.ioasysapp.config.RetrofitConfig;
import com.example.ioasysapp.domain.Empresas;
import com.example.ioasysapp.interfaces.RecyclerViewOnClickListenerHack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainPresenter implements MainContrato.MainPresenter, RecyclerViewOnClickListenerHack {
    private MainContrato.MainView view;
    private List<Empresas> list;

    public MainPresenter(MainContrato.MainView view){
        this.view = view;
    }

    @Override
    public void start() {
        view.bindView();
    }

    @Override
    public void pesquisaEmpresas(String search, String token, String client, String uid) {
        view.showMessage(View.GONE);
        view.showMessageVazio(View.GONE);
        view.showProgressBar(View.VISIBLE);

        view.removeViewList();

        Call<ResponseBody> call = new RetrofitConfig()
                .getApiService()
                .getListEmpresas(search, token, client, uid);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String body = response.body().string();
                    Log.i("listEmpresas", body);

                    list = getListEmpresas(body);
                    EmpresasAdapter adapter = new EmpresasAdapter((Context)view, list);
                    adapter.setRecyclerViewOnClickListenerHack(MainPresenter.this);

                    view.setEmpresas(adapter);

                    if (list.isEmpty())
                        view.showMessageVazio(View.VISIBLE);

                    view.showProgressBar(View.GONE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private List<Empresas> getListEmpresas(String body) {
        List<Empresas> list = new ArrayList<>();

        try {
            JSONObject objectArray = new JSONObject(body);
            JSONArray jsonArray = objectArray.getJSONArray("enterprises");
            JSONObject jsonObject;

            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObject = jsonArray.getJSONObject(i);

                String nome = jsonObject.getString("enterprise_name");
                String foto = jsonObject.getString("photo");
                String pais = jsonObject.getString("country");
                String descricao = jsonObject.getString("description");
                String tipo = jsonObject.getJSONObject("enterprise_type").getString("enterprise_type_name");

                Empresas empresas = new Empresas(nome, tipo, pais, foto, descricao);
                list.add(empresas);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void onClickListener(View view, int position) {
        this.view.detalhesEmpresa(list.get(position));
    }
}
