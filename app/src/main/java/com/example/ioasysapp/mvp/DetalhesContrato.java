package com.example.ioasysapp.mvp;

import com.example.ioasysapp.domain.Empresas;

public interface DetalhesContrato {
    interface DetalhesView{
        void bindView();
        void detalhes(String nome, String foto, String descricao);
        void finishActivity();
    }

    interface DetalhesPresenter{
        void start();
        void mostrarDetalhes(Empresas empresas);
        void optionsItem(int id);
    }
}
