package com.example.ioasysapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ioasysapp.domain.Empresas;
import com.example.ioasysapp.mvp.DetalhesContrato;
import com.example.ioasysapp.mvp.DetalhesPresenter;
import com.squareup.picasso.Picasso;

public class DetalhesActivity extends AppCompatActivity implements DetalhesContrato.DetalhesView {
    public static final String EXTRA_EMPRESA = "empresa";

    private Toolbar tbEmpresa;
    private ImageView ivEmpresa;
    private TextView tvDetalhes;

    private DetalhesContrato.DetalhesPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes);

        final Empresas empresas = (Empresas) getIntent().getSerializableExtra(EXTRA_EMPRESA);

        presenter = new DetalhesPresenter(this);
        presenter.start();

        presenter.mostrarDetalhes(empresas);
    }

    @Override
    public void bindView() {
        tbEmpresa = findViewById(R.id.toolbar_detalhe_empresa);
        ivEmpresa = findViewById(R.id.iv_detalhe_empresa);
        tvDetalhes = findViewById(R.id.tv_detalhe_emoresa);
    }

    @Override
    public void detalhes(String nome, String foto, String descricao) {
        tbEmpresa.setTitle(nome);
        setSupportActionBar(tbEmpresa);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvDetalhes.setText(descricao);
        Picasso.get()
                .load(foto)
                .into(ivEmpresa);
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        presenter.optionsItem(id);

        return true;
    }
}