package com.example.ioasysapp.mvp;

import android.content.Intent;

import com.example.ioasysapp.adapter.EmpresasAdapter;
import com.example.ioasysapp.domain.Empresas;

public interface MainContrato {

    interface MainView{
        void bindView();
        void setEmpresas(EmpresasAdapter adapter);
        void removeViewList();
        void showProgressBar(int visibility);
        void showMessageVazio(int visibility);
        void showMessage(int visibility);
        void detalhesEmpresa(Empresas empresa);
    }

    interface MainPresenter{
        void start();
        void pesquisaEmpresas(String search, String token, String client, String uid);
    }
}
