package com.example.ioasysapp.mvp;

import com.example.ioasysapp.domain.Empresas;

public class DetalhesPresenter implements DetalhesContrato.DetalhesPresenter {
    private DetalhesContrato.DetalhesView view;

    public DetalhesPresenter(DetalhesContrato.DetalhesView view) {
        this.view = view;
    }

    @Override
    public void start() {
        view.bindView();
    }

    @Override
    public void mostrarDetalhes(Empresas empresas) {
        String nome = empresas.getNome();
        String foto = empresas.getImage();
        String descricao = empresas.getDescricao();

        view.detalhes(nome, foto, descricao);
    }

    @Override
    public void optionsItem(int id) {
        switch (id) {
            case android.R.id.home:
                view.finishActivity();
                break;
        }
    }
}
