package com.example.ioasysapp.mvp;

import android.content.Intent;

public interface LoginContrato {
    interface LoginView {
        void bindView();

        void mostrarErro(String message);

        void loginSuccess(Intent intent);

        void loginErro();

        void emailErro(String message);

        void passwordErro(String message);

        void telaCarregamento();
    }

    interface LoginPresenter {
        void start();

        void setView(LoginView view);

        void validaLogin(String email, String senha);
    }
}
