package com.example.ioasysapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ioasysapp.adapter.EmpresasAdapter;
import com.example.ioasysapp.domain.Empresas;
import com.example.ioasysapp.mvp.MainContrato;
import com.example.ioasysapp.mvp.MainPresenter;

public class MainActivity extends AppCompatActivity implements MainContrato.MainView {

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private TextView tvMessage;
    private TextView tvMessageVazio;
    private ProgressBar progressBar;

    private MainContrato.MainPresenter presenter;

    private String token;
    private String client;
    private String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new MainPresenter(this);

        presenter.start();

        Intent intent = getIntent();
        token = intent.getStringExtra("token");
        client = intent.getStringExtra("client");
        uid = intent.getStringExtra("uid");

        Log.i("dados", "token: "+token+"\nclient: "+client+"\nuid: "+uid);
    }

    @Override
    public void bindView() {
        toolbar = findViewById(R.id.toolbar_home);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        tvMessage = findViewById(R.id.message_incial);
        tvMessageVazio = findViewById(R.id.tv_messagem_vazio);
        progressBar = findViewById(R.id.progress_home);
        recyclerView = findViewById(R.id.rv_empresas);

        progressBar.setVisibility(View.GONE);
        tvMessageVazio.setVisibility(View.GONE);

        recyclerView.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
    }

    @Override
    public void setEmpresas(EmpresasAdapter adapter) {
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void removeViewList() {
        recyclerView.removeAllViews();
    }

    @Override
    public void showProgressBar(int visibility) {
        progressBar.setVisibility(visibility);
    }

    @Override
    public void showMessageVazio(int visibility) {
        tvMessageVazio.setVisibility(visibility);
    }

    @Override
    public void showMessage(int visibility) {
        tvMessage.setVisibility(visibility);
    }

    @Override
    public void detalhesEmpresa(Empresas empresa) {
        Intent intent = new Intent(MainActivity.this, DetalhesActivity.class);
        intent.putExtra(DetalhesActivity.EXTRA_EMPRESA, empresa);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Acontece quando clica o botão de pesquisa
                String search = searchView.getQuery().toString();

                //Talvez tenho que passar a intent
                presenter.pesquisaEmpresas(search, token, client, uid);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Acontece enquanto digita
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }
}